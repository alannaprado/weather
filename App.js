import { StatusBar } from 'expo-status-bar';
import { Keyboard, KeyboardAvoidingView, ImageBackground, StyleSheet, Text, View, TextInput, Button, Image, Pressable } from 'react-native';
import axios from "axios";
import React, { useState } from 'react';
import Myimage from './temperature-white.png'

const image = { uri: "https://reactjs.org/logo-og.png" };


export default function App() {
  const [city, setCity] = useState("")
  const [items, setItems] = useState([])
  const [DataisLoaded, setData] = useState()


  const handleClick = async () => {
    try {
      if (!city) {
        alert("You need to enter a city!")
        return setData(false)
      }
      const res = await axios.get(`https://api.openweathermap.org/data/2.5/weather?q=` + city + `&units=metric&APPID=314fa871cde9c1c085c0b0e40e924553`)
      console.log(res.data)
      setItems(res.data)
      setData(res.data)
    } catch (err) {
      alert("City does not exist")

    }
  }


  const clear = () => {
    setItems([])
    setCity('')
    setData(false)
  }

  if (!DataisLoaded) {
    return (
<KeyboardAvoidingView style={styles.container} behavior="height">
      <View style={styles.container}>
        <Pressable style={styles.pressable} onPress={Keyboard.dismiss}>
          <Text style={styles.title}>Weather</Text>
          <Image style={styles.image} source={Myimage} />
          <TextInput placeholder=" Enter a city..." style={styles.input} onChangeText={(city) => setCity(city)} />
          <Pressable style={styles.button} onPress={handleClick} >
            <Text style={styles.buttontitle}>Search</Text>
          </Pressable>
        </Pressable>
      </View>
      </KeyboardAvoidingView>
    );
  } else {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>Weather in</Text>
        <Text style={styles.title}>{city} </Text>

        <View style={styles.containerpink}>
          <Text style={styles.text}>Now: {items.main.temp}&deg;C </Text>
        </View>
        <View style={styles.containerpink}>
          <Text style={styles.text}> Min: {items.main.temp_min}&deg;C</Text>
        </View>
        <View style={styles.containerpink}>
          <Text style={styles.text}>Max: {items.main.temp_max}&deg;C</Text>
        </View>
        <Pressable style={styles.button} onPress={clear} >
          <Text style={styles.buttontitle}>New Search</Text>
        </Pressable>

      </View>
    )
  }


}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  pressable: {
    flex: 1,
    backgroundColor: "white",
    width: "100%",
    alignItems: 'center',
    justifyContent: 'center',
  },

  containerpink: {
    flexDirection: "column",
    backgroundColor: "#ffb6c1",
    alignItems: 'center',
    justifyContent: 'center',
    height: 80,
    width: 200,
    padding: 20,
    margin: 30,
  },
  input: {
    height: 40,
    borderColor: "gray",
    width: 200,
    borderWidth: 1,
    marginBottom: 10,
    justifyContent: 'center',
    color: "black",
    fontSize: 16,
    padding: 5,
  },
  title: {
    fontSize: 40,
    fontWeight: "bold",
    color: "black",
  },
  text: {
    fontSize: 20,
    fontWeight: "bold",
    color: "white",
  },
  buttontitle: {
    fontSize: 20,
    fontWeight: "bold",
    color: "black",
  },
  image: {
    justifyContent: "center",
    width: 200,
    height: 200,
    alignItems: 'center',
    marginTop: 50,
    marginBottom: 50,
  },
  button: {
    borderColor: "black",
    borderWidth: 0,
    height: 50,
    width: 160,
    backgroundColor: "#add8ec",
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 12,
    paddingHorizontal: 22,
    borderRadius: 8,
    elevation: 3,
  }
});